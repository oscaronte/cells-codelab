import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './bbva-component-styles.js';
import '@bbva-web-components/bbva-form-text/bbva-form-text.js';
import '@bbva-web-components/bbva-form-password/bbva-form-password.js';
import '@bbva-web-components/bbva-button-default/bbva-button-default.js';
import '@bbva-web-components/bbva-progress-content/bbva-progress-content.js';
/**
This component ...

Example:

```html
<bbva-component></bbva-component>
```

##styling-doc

@customElement bbva-component
@polymer
@LitElement
@demo demo/index.html
*/
export class BbvaComponent extends LitElement {
  static get is() {
    return 'bbva-component';
  }

  // Declare properties
  static get properties() {
    return {
      userInputLabel: {
        type: String,
        attribute: 'user-input-label'
      },
      userInputValue: {
        type: String,
        attribute: 'user-input-value'
      },
      passwordInputLabel: {
        type: String,
        attribute: 'password-input-label'
      },
      passwordInputValue: {
        type: String,
        attribute: 'password-input-value'
      },
      buttonText: {
        type: String,
        attribute: 'button-text'
      },
      loading: {
        type: Boolean
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.userInputLabel = 'Username';
    this.passwordInputLabel = 'Password';
    this.buttonText = 'Submit';
    this.loading = false;
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('bbva-component-shared-styles')
    ]
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <form ?hidden="${this.loading}">
        <bbva-form-text
          label="${this.userInputLabel}"
          .value="${this.userInputValue}"
          @input="${(e) => this.userInputValue = e.target.value}"
          type="text"></bbva-form-text>
        <bbva-form-password
          label="${this.passwordInputLabel}"
          .value="${this.passwordInputValue}"
          @input="${(e) => this.passwordInputValue = e.traget.value}"
          type="password"></bbva-form-password>
        <bbva-button-default type="submit"
          ?disabled="${!this.userInputValue || !this.passwordInputValue}"
          @click="${this.buttonText}">
          ${this.buttonText}</bbva-button-default>
      </form>
      <bbva-progress-content ?hidden="${!this.loading}"></bbva-progress-content>
    `;
  }

  _submit(ev) {
    ev.preventDefault();
    this.loading = true;
    this.dispatchEvent(new CustomEvent('bbva-component-submit', {
      bubbles: true,
      composed: true,
      detail: {
        username: this.userInputValue,
        password: this.passwordInputValue
      }
    }))
  }
}

// Register the element with the browser
customElements.define(BbvaComponent.is, BbvaComponent);
